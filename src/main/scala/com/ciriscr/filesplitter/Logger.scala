package com.ciriscr.filesplitter

import java.io.PrintWriter

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 23/07/13
 * Time: 11:19 AM
 */

class Logger(filepath: String) {

  private val out: PrintWriter = new PrintWriter(filepath)

  def escribir(mensaje: String){
    out.println(mensaje)
    flush
  }

  def escribir(e: Exception){
    escribir(e.getMessage)
    e.getStackTrace.foreach(t => escribir("\t at " + t.toString))
  }

  def cerrar {out.close()}
  def flush {out.flush()}

}