package com.ciriscr.filesplitter

import scala.collection.mutable.Map

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 23/07/13
 * Time: 11:13 AM
 */

object Main {

  val params = Map.empty[Parametros.Value, Any]

  def main(a: Array[String]) {
    val it = a.toIterator
    while (it.hasNext){
      val arg = it.next()
      arg match {
        case "-s" => params += (Parametros.fileSize -> it.next())
        case "-o" => params += (Parametros.salida -> it.next())
        case "-h" => params += (Parametros.header -> true)
        case "-fh" => params += (Parametros.firstLineHeader -> true)
        case e @ _ => params += (Parametros.entrada -> e)
      }
    }

    Reader(params.get(Parametros.firstLineHeader).map(_.toString.toBoolean).exists(b => b),
           params.get(Parametros.fileSize).map(_.toString.toInt).getOrElse(0),
           params.get(Parametros.entrada).map(_.toString).getOrElse(""),
           params.get(Parametros.salida).map(_.toString),
           params.get(Parametros.header).map(_.toString.toBoolean).exists(b => b)).read()
  }

}

object Parametros extends Enumeration {
  val entrada = Value
  val salida = Value
  val header = Value
  val firstLineHeader = Value
  val fileSize = Value
}
